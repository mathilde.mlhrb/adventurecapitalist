/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.AdventureCapitalist;

import generated.PallierType;
import generated.ProductType;
import generated.World;
import java.io.FileNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBException;

/**
 *
 * @author cleme
 */
@Path("generic")
public class Webservice {

    Services services;

    public Webservice() {
        services = new Services();
    }

    @GET
    @Path("world")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getWorld(@Context HttpServletRequest request) throws JAXBException, FileNotFoundException {
        String username = request.getHeader("X-user");
        return Response.ok(services.getWorld(username)).build();
    }

    @PUT
    @Path("product")
    @Consumes({MediaType.APPLICATION_JSON})
    public Boolean updateProduct(@Context HttpServletRequest request, ProductType product) throws JAXBException, FileNotFoundException {
        String username = request.getHeader("X-user");
        Boolean action = services.updateProduct(username, product);
        return action;

    }

    @PUT
    @Path("manager")
    @Consumes({MediaType.APPLICATION_JSON})
    public Boolean updateManager(@Context HttpServletRequest request, PallierType manager) throws JAXBException, FileNotFoundException {
        String username = request.getHeader("X-user");
        Boolean action = services.updateManager(username, manager);
        return action;

    }

    @PUT
    @Path("upgrade")
    @Consumes({MediaType.APPLICATION_JSON})
    public Boolean updateUpgrade(@Context HttpServletRequest request, PallierType upgrade) throws JAXBException, FileNotFoundException {
        String username = request.getHeader("X-user");
        Boolean action = services.updateUpgrade(username, upgrade);
        return action;

    }

    @DELETE
    @Path("world")
    @Consumes({MediaType.APPLICATION_JSON})
    public Boolean reset(@Context HttpServletRequest request) throws JAXBException, FileNotFoundException {
        String username = request.getHeader("X-user");
        Boolean action = services.reset(username);
        return action;
    }

    @PUT
    @Path("upgradeangel")
    @Consumes({MediaType.APPLICATION_JSON})
    public Boolean updateUpgradeAngel(@Context HttpServletRequest request, PallierType upgradeAngel) throws JAXBException, FileNotFoundException {
        String username = request.getHeader("X-user");
        Boolean action = services.updateUpgradeAngel(username, upgradeAngel);
        return action;

    }
}
