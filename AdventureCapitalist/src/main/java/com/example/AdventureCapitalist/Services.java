/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.AdventureCapitalist;

import generated.PallierType;
import generated.ProductType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import generated.World;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author cleme
 */
public class Services {

    public World readWorldFromXML(String username) throws JAXBException, FileNotFoundException {
        World world = new World();
        InputStream input = null;
        try {
            input = new FileInputStream(username + "-world.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (input == null) {
            input = getClass().getClassLoader().getResourceAsStream("world.xml");
        }
        try {
            JAXBContext cont = JAXBContext.newInstance(World.class);
            Unmarshaller u = cont.createUnmarshaller();
            world = (World) u.unmarshal(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return world;

    }

    public void saveWordlToXml(World world, String username) throws JAXBException, FileNotFoundException {
        OutputStream output = new FileOutputStream(username + "-world.xml");
        JAXBContext cont = JAXBContext.newInstance(World.class);
        Marshaller m = cont.createMarshaller();
        m.marshal(world, output);
    }

    public World getWorld(String username) throws JAXBException, FileNotFoundException {
        World oldworld = readWorldFromXML(username);
        saveWordlToXml(oldworld, username);
        World newworld = updateScore(username);
        saveWordlToXml(newworld, username);
        return newworld;

    }

    public PallierType findManagerByName(World world, String name) {
        for (PallierType manager : world.getManagers().getPallier()) {
            if (manager.getName().equals(name)) {
                return manager;
            }
        }
        return null;
    }

    public ProductType findProductById(World world, int id) {
        for (ProductType product : world.getProducts().getProduct()) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }

    // prend en paramètre le pseudo du joueur et le produit
    // sur lequel une action a eu lieu (lancement manuel de production ou
    // achat d’une certaine quantité de produit)
    // renvoie false si l’action n’a pas pu être traitée
    public Boolean updateProduct(String username, ProductType newproduct) throws JAXBException, FileNotFoundException {
        // aller chercher le monde qui correspond au joueur
        World world = getWorld(username);
        // trouver dans ce monde, le produit équivalent à celui passé en paramètre
        ProductType product = findProductById(world, newproduct.getId());
        if (product == null) {
            return false;
        }
        // calculer la variation de quantité. Si elle est positive c'est
        // que le joueur a acheté une certaine quantité de ce produit
        // sinon c’est qu’il s’agit d’un lancement de production.
        System.out.println("log :" + newproduct.getQuantite());
        int qtchange = newproduct.getQuantite() - product.getQuantite();
        if (qtchange > 0) {
            // soustraire de l'argent du joueur le cout de la quantité
            double money = world.getMoney();
            System.out.println("log :" + money);
            double coutUnitaire = product.getCout();
            System.out.println("log :" + coutUnitaire);
            double croissance = product.getCroissance();
            System.out.println("log :" + croissance);
            double coutTotal = (coutUnitaire * (1 - Math.pow(croissance, qtchange))) / (1 - croissance);
            System.out.println("log :" + coutTotal);
            world.setMoney(money - coutTotal);
            System.out.println("log :" + world.getMoney());
            double newCout = coutUnitaire * Math.pow(croissance, qtchange);
            product.setCout(newCout);

            // achetée et mettre à jour la quantité de product
            product.setQuantite(newproduct.getQuantite());
        } else {
            // initialiser product.timeleft à product.vitesse
            // pour lancer la production
            product.setTimeleft(product.getVitesse());
            //world.setLastupdate(System.currentTimeMillis());
        }

        //unlock
        for (PallierType pallier : product.getPalliers().getPallier()) {
            if (!pallier.isUnlocked()) {
                if (product.getQuantite() >= pallier.getSeuil()) {
                    System.out.println("pallier");
                    pallier.setUnlocked(true);
                    if (pallier.getTyperatio().value().equals("vitesse")) {
                        product.setVitesse((int) (product.getVitesse() / pallier.getRatio()));
                        System.out.println("vitesse");
                    }
                    if (pallier.getTyperatio().value().equals("gain")) {
                        product.setRevenu(product.getRevenu() * pallier.getRatio());
                        System.out.println("gain");
                    }
                    if (pallier.getTyperatio().value().equals("ange")) {
                        world.setAngelbonus((int) (world.getAngelbonus() + pallier.getRatio()));
                        System.out.println("ange");
                    }
                }
                break;
            }
        }

        saveWordlToXml(world, username);

        //allunlock
        for (PallierType pallier2 : world.getAllunlocks().getPallier()) {
            if (!pallier2.isUnlocked()) {
                System.out.println("log :" + !pallier2.isUnlocked());
                double qte = pallier2.getSeuil();
                for (ProductType product2 : world.getProducts().getProduct()) {
                    System.out.println("quantité produit :" + product2.getQuantite());
                    if (product2.getQuantite() < qte) {
                        qte = product2.getQuantite();
                    }
                }
                System.out.println("quantité :" + qte);
                if (qte >= pallier2.getSeuil()) {
                    pallier2.setUnlocked(true);
                    if (pallier2.getTyperatio().value().equals("vitesse")) {
                        for (ProductType product2 : world.getProducts().getProduct()) {
                            product2.setVitesse((int) (product2.getVitesse() / pallier2.getRatio()));
                        }

                    }
                    if (pallier2.getTyperatio().value().equals("gain")) {
                        for (ProductType product2 : world.getProducts().getProduct()) {
                            product2.setRevenu(product2.getRevenu() * pallier2.getRatio());
                        }
                    }
                    if (pallier2.getTyperatio().value().equals("ange")) {
                        world.setAngelbonus((int) (world.getAngelbonus() + pallier2.getRatio()));
                    }
                }

            }
        }

        /*sauvegarder les changements du monde*/
        saveWordlToXml(world, username);
        return true;
    }

    // prend en paramètre le pseudo du joueur et le manager acheté.
    // renvoie false si l’action n’a pas pu être traitée
    public Boolean updateManager(String username, PallierType newmanager) throws JAXBException, FileNotFoundException {
        // aller chercher le monde qui correspond au joueur
        World world = getWorld(username);
        // trouver dans ce monde, le manager équivalent à celui passé
        // en paramètre
        System.out.println("log :" + newmanager.getName());
        PallierType manager = findManagerByName(world, newmanager.getName());
        //System.out.println("log :"+manager.getName());
        if (manager == null) {
            return false;
        }
        //System.out.println("log :"+manager.getName());

        // débloquer ce manager
        manager.setUnlocked(true);

        // trouver le produit correspondant au manager
        ProductType product = findProductById(world, manager.getIdcible());
        //System.out.println("log :"+product.getId());
        if (product == null) {
            return false;
        }
        // débloquer le manager de ce produit
        product.setManagerUnlocked(true);

        // soustraire de l'argent du joueur le cout du manager
        double money = world.getMoney();
        double cout = manager.getSeuil();
        world.setMoney(money - cout);

        // sauvegarder les changements au monde
        saveWordlToXml(world, username);
        return true;
    }

    public World updateScore(String username/*, World world*/) throws JAXBException, FileNotFoundException {
        World world = readWorldFromXML(username);
        if (world.getLastupdate() == 0) {
            world.setLastupdate(System.currentTimeMillis());
        }
        double score = world.getScore();
        double money = world.getMoney();
        long tempsCourant = System.currentTimeMillis();
        long tempsEcoule = tempsCourant - world.getLastupdate();
        int angelBonus = world.getAngelbonus();
        double activeAngels = world.getActiveangels();
        System.out.println(tempsCourant);
        System.out.println(tempsEcoule);

        //parcourir chaque produit et pour chaque calculer combien d'exmplaire a été produit depuis la dernière mise à jour
        for (ProductType product : world.getProducts().getProduct()) {
            Boolean manager = product.isManagerUnlocked();
            System.out.println(manager);
            //Si ce produit n’a pas de manager, il suffit de vérifier que timeleft n’est pas nul, et qu’il est
            //inférieur au temps écoulé. Si c’est le cas, un produit a été créé (et donc on ajoute les gains au score),
            //sinon on met à jour timeleft en en soustrayant le temps écoulé.
            if (!manager) {
                if (product.getTimeleft() != 0 && product.getTimeleft() < tempsEcoule) {
                    score = score + (product.getRevenu() * product.getQuantite() * (1 + activeAngels * angelBonus / 100));
                    money = money + (product.getRevenu() * product.getQuantite() * (1 + activeAngels * angelBonus / 100));
                    product.setTimeleft(0);
                } else {
                    if (product.getTimeleft() != 0) {
                        product.setTimeleft(product.getTimeleft() - tempsEcoule);
                    }
                }
            }

            //Si ce produit a un manager, c’est plus compliqué car il faut calculer combien de fois sa production
            //complète a pu se produire depuis la dernière mise à jour, et mettre à jour le timeleft du produit
            //en conséquence. 
            if (manager) {
                if (product.getTimeleft() > tempsEcoule) {
                    product.setTimeleft(product.getTimeleft() - tempsEcoule);
                } else {
                    int nbCycle = (int) Math.floor(tempsEcoule / product.getVitesse());
                    //récupere reste temps pour définir timeleft
                    product.setTimeleft(tempsEcoule - (product.getVitesse() * nbCycle));
                    //mettre à jour le score
                    score = score + (nbCycle * product.getRevenu() * product.getQuantite() * (1 + activeAngels * angelBonus / 100));
                    money = money + (nbCycle * product.getRevenu() * product.getQuantite() * (1 + activeAngels * angelBonus / 100));
                    System.out.println("manager");
                }
            }
            world.setScore(score);
            world.setMoney(money);
            System.out.println(world.getScore());

        }
        world.setLastupdate(tempsCourant);
        return world;
    }

    public Boolean updateUpgrade(String username, PallierType upgrade) throws JAXBException, FileNotFoundException {
        // aller chercher le monde qui correspond au joueur
        World world = getWorld(username);

        //appliquer l'effet de l'upgrade
        for (PallierType upgradeWorld : world.getUpgrades().getPallier()) {
            effet(upgrade, world, username, upgradeWorld);
        }

        // soustraire de l'argent du joueur le cout du manager
        double money = world.getMoney();
        double cout = upgrade.getSeuil();
        world.setMoney(money - cout);

        // sauvegarder les changements au monde
        saveWordlToXml(world, username);
        return true;

    }

    public Boolean reset(String username) throws JAXBException, FileNotFoundException {
        // aller chercher le monde qui correspond au joueur
        World world = getWorld(username);

        double totalAngel = world.getTotalangels();
        double activeAngel = world.getActiveangels();
        double score = world.getScore();
        double nbAnge = 150 * Math.sqrt(score / Math.pow(10, 8)) - totalAngel;

        //récuperer monde initial
        World worldInit = readWorldFromXML("");

        worldInit.setTotalangels(nbAnge + totalAngel);
        worldInit.setActiveangels(nbAnge + activeAngel);
        worldInit.setScore(score);

        saveWordlToXml(worldInit, username);

        return true;
    }

    public Boolean updateUpgradeAngel(String username, PallierType upgrade) throws JAXBException, FileNotFoundException {
        // aller chercher le monde qui correspond au joueur
        World world = getWorld(username);

        //Parcourir les upgradeAngel du monde et trouver la correspondance avec le nom d'entrée
        for (PallierType upgradeAngel : world.getAngelupgrades().getPallier()) {
            effet(upgrade, world, username, upgradeAngel);
        }

        // soustraire de l'argent du joueur le cout
        double ange = world.getActiveangels();
        double cout = upgrade.getSeuil();
        world.setActiveangels(ange - cout);

        // sauvegarder les changements au monde
        saveWordlToXml(world, username);
        return true;

    }

    public void effet(PallierType newUpgrade, World world, String username, PallierType upgrade) throws JAXBException, FileNotFoundException {

        System.out.println(newUpgrade.getName().equals(upgrade.getName()));
        if (newUpgrade.getName().equals(upgrade.getName())) {
            upgrade.setUnlocked(true);
            //si l'upagrade s'applique à tous les produits
            if (newUpgrade.getIdcible() == 0) {
                System.out.println("tous les produits");
                if (newUpgrade.getTyperatio().value().equals("vitesse")) {
                    System.out.println("vitesse");
                    for (ProductType product : world.getProducts().getProduct()) {
                        product.setVitesse((int) (product.getVitesse() / newUpgrade.getRatio()));
                    }

                }
                if (newUpgrade.getTyperatio().value().equals("gain")) {
                    System.out.println("gain");
                    for (ProductType product : world.getProducts().getProduct()) {
                        product.setRevenu(product.getRevenu() * newUpgrade.getRatio());
                    }
                }
                if (newUpgrade.getTyperatio().value().equals("ange")) {
                    System.out.println("ange");
                    world.setAngelbonus((int) (world.getAngelbonus() * newUpgrade.getRatio()));
                }
            } //si l'upgrade concerne les anges
            else if (newUpgrade.getIdcible() == -1) {
                System.out.println("ange");
                world.setAngelbonus((int) (world.getAngelbonus() + newUpgrade.getRatio()));
                saveWordlToXml(world, username);

            } //si l'upgrade s'applique à un seul produit
            else {
                for (ProductType product : world.getProducts().getProduct()) {
                    System.out.println("un seul produit");
                    if (product.getId() == newUpgrade.getIdcible()) {
                        if (newUpgrade.getTyperatio().value().equals("vitesse")) {
                            System.out.println("vitesse");
                            product.setVitesse((int) (product.getVitesse() / newUpgrade.getRatio()));
                            saveWordlToXml(world, username);
                        }
                        if (newUpgrade.getTyperatio().value().equals("gain")) {
                            System.out.println("gain");
                            product.setRevenu(product.getRevenu() * newUpgrade.getRatio());
                            saveWordlToXml(world, username);
                        }
                    }
                }
            }
        }
    }
}
